package lowker.nasiqulibat.id.lowker.Models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Nasiqul on 2/12/2018.
 */

public class GetKategori {
    @SerializedName("status")
    String status;
    @SerializedName("result")
    List<Kategori> listDataKtg;
    @SerializedName("message")
    String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Kategori> getListDataKtg() {
        return listDataKtg;
    }

    public void setListDataKtg(List<Kategori> listDataKtg) {
        this.listDataKtg = listDataKtg;
    }
}
