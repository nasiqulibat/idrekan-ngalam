package lowker.nasiqulibat.id.lowker;

import android.app.SearchManager;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.List;

import lowker.nasiqulibat.id.lowker.Adapter.Lowker2Adapter;
import lowker.nasiqulibat.id.lowker.Models.GetLowker;
import lowker.nasiqulibat.id.lowker.Models.Lowker;
import lowker.nasiqulibat.id.lowker.Models.PostPutDelLowker;
import lowker.nasiqulibat.id.lowker.Rest.ApiClient;
import lowker.nasiqulibat.id.lowker.Rest.ApiInterface;
import lowker.nasiqulibat.id.lowker.Rest.Session;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Main3Activity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    public static Main3Activity ma3;
    Context mContext;
    TextView tvId, tvUsername;
    ApiInterface mApiInterface;
    Session session;
    String username, search;
    ImageView imgP;
    int intentFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        tvId = (TextView) findViewById(R.id.tvId);
        mContext = this;

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        session = new Session(getApplicationContext());

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerView = navigationView.getHeaderView(0);
        tvUsername = (TextView) headerView.findViewById(R.id.tvUsername);
        imgP = (ImageView) headerView.findViewById(R.id.imgProfil);

        Menu nav_Menu = navigationView.getMenu();


        mApiInterface = ApiClient.getClient().create(ApiInterface.class);

        username = session.getusename();
        if (session.getid_user().isEmpty()) {
            tvUsername.setText("Anda belum Login");
            nav_Menu.findItem(R.id.nav_profil).setVisible(false);
            nav_Menu.findItem(R.id.nav_iklan).setVisible(false);
            nav_Menu.findItem(R.id.nav_logout).setVisible(false);
            imgP.setClickable(true);
            imgP.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(Main3Activity.this, LoginActivity.class);
                    startActivity(i);
                }
            });

//            MenuItem st = (MenuItem) findViewById(R.id.nav_logout);
//            st.setVisible(false);
        } else {
            imgP.setClickable(false);
            Picasso.with(mContext).load("http://idrekan-ngalam.000webhostapp.com/assets/upload/" + session.get_profil()).into(imgP);
            tvUsername.setText(username);
        }

        FragmentManager manager = getSupportFragmentManager();

        if (getIntent().getExtras() != null)
        {
            intentFragment = getIntent().getExtras().getInt("fragment");

            switch (intentFragment) {
                case 1:
                    Bundle bundle = new Bundle();
                    bundle.putString("keyword", getIntent().getExtras().getString("keyword"));
                    bundle.putString("kategori", getIntent().getExtras().getString("kategori"));
                    bundle.putString("sort", getIntent().getExtras().getString("sort"));

                    // set Fragmentclass Arguments
                    Search2Fragment search2Fragment = new Search2Fragment();
                    search2Fragment.setArguments(bundle);

                    android.support.v4.app.FragmentTransaction ft = manager.beginTransaction();
                    ft.replace(R.id.cm3_fragment, search2Fragment, "Search2");
                    ft.addToBackStack("main");
                    ft.commit();

                    break;
                default:
                    break;
            }
        }
        else {
            IklanFragment iklanFragment = new IklanFragment();
            manager.beginTransaction().add(R.id.cm3_fragment, iklanFragment, "home").commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (getSupportFragmentManager().findFragmentByTag("home") != null) {
                getSupportFragmentManager().popBackStack("main",
                        FragmentManager.POP_BACK_STACK_INCLUSIVE);
            } else {
                super.onBackPressed();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.search, menu);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.menu_search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                Bundle bundle = new Bundle();
                bundle.putString("textS", query);

                search = query;
                // set Fragmentclass Arguments
                SearchFragment searchFragment = new SearchFragment();
                searchFragment.setArguments(bundle);

                FragmentManager manager = getSupportFragmentManager();
                android.support.v4.app.FragmentTransaction ft = manager.beginTransaction();
                ft.replace(R.id.cm3_fragment, searchFragment, "Search");
                ft.addToBackStack("main");
                ft.commit();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                //TODO write your code what you want to perform on search text change
                return true;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.menu_filter) {
            Intent i = new Intent(Main3Activity.this, FilterActivity.class);
            i.putExtra("string", search);
            startActivity(i);

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            IklanFragment iklanFragment = new IklanFragment();
            FragmentManager manager = getSupportFragmentManager();
            manager.beginTransaction().replace(R.id.cm3_fragment, iklanFragment, "home").commit();
        } else if (id == R.id.nav_profil) {
            Intent inten = new Intent(Main3Activity.this, ProfilActivity.class);
            startActivity(inten);
        } else if (id == R.id.nav_iklan) {
            IklanSayaFragment iklanSayaFragment = new IklanSayaFragment();

            FragmentManager manager = getSupportFragmentManager();
            android.support.v4.app.FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.cm3_fragment, iklanSayaFragment, "iklan");
            ft.addToBackStack("main");
            ft.commit();
        } else if (id == R.id.nav_logout) {
            session.setid_user("");
            session.setusename("");
            Intent intent = getIntent();
            finish();
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void delete(String id)
    {
        Call<PostPutDelLowker> lowkerCall = mApiInterface.deleteLowker(id);
        lowkerCall.enqueue(new Callback<PostPutDelLowker>() {
            @Override
            public void onResponse(Call<PostPutDelLowker> call, Response<PostPutDelLowker>
                    response) {
                Log.d("Retrofit Get", "Delete data Pekerjaan");
                Toast.makeText(mContext, "Delete data Pekerjaan", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<PostPutDelLowker> call, Throwable t) {
                Log.e("Retrofit Delete", t.toString());
            }
        });
    }
}
