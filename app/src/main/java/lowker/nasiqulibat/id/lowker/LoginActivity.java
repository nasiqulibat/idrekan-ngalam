package lowker.nasiqulibat.id.lowker;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

import lowker.nasiqulibat.id.lowker.Models.User;
import lowker.nasiqulibat.id.lowker.Rest.ApiClient;
import lowker.nasiqulibat.id.lowker.Rest.Session;
import lowker.nasiqulibat.id.lowker.Rest.UserInterface;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    Button btnLogin, btnDaftar;
    EditText edtUser, edtPass;
    UserInterface mUserInterface;
    private Session session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        session = new Session(getApplicationContext());

        edtUser = (EditText) findViewById(R.id.edtUsername);
        edtPass = (EditText) findViewById(R.id.edtPassword);
        btnLogin = (Button) findViewById(R.id.btnLogin);

        mUserInterface = ApiClient.getClient().create(UserInterface.class);


        btnLogin.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if (edtUser.getText().toString().equals("")) {
                    Toast.makeText(LoginActivity.this, "Username Wajib Di Isi!", Toast.LENGTH_LONG).show();
                } else if (edtPass.getText().toString().equals("")) {
                    Toast.makeText(LoginActivity.this, "Password Wajib Di Isi!", Toast.LENGTH_LONG).show();
                } else {
                    mUserInterface = ApiClient.getClient().create(UserInterface.class);
                    retrofit2.Call<List<User>> loginCall = mUserInterface.cekLogin(edtUser.getText().toString(), edtPass.getText().toString());
                    loginCall.enqueue(new Callback<List<User>>() {
                        @Override
                        public void onResponse(retrofit2.Call<List<User>> call, Response<List<User>> response) {
                            if (response.isSuccessful()) {
                                List<User> login = response.body();
                                if (login.get(0).getUsername().equals(edtUser.getText().toString())) {
                                    //Pindah Activity
                                    session.setusename(login.get(0).getUsername());
                                    session.setid_user(login.get(0).getIdUser());
                                    session.setprofil(login.get(0).getProfil());
                                    Intent i = new Intent(LoginActivity.this, Main3Activity.class);
                                    finish();
                                    startActivity(i);
                                } else {
                                    Toast.makeText(LoginActivity.this, "Username Atau Password Salah", Toast.LENGTH_LONG).show();
                                }
                            } else {
                                Toast.makeText(LoginActivity.this, "Username Atau Password Salah", Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onFailure(retrofit2.Call<List<User>> call, Throwable t) {
                            Toast.makeText(LoginActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        });


        btnDaftar = (Button) findViewById(R.id.btnDaftar);
        btnDaftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, DaftarActivity.class));
            }
        });
    }
}
