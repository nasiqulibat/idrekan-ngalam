package lowker.nasiqulibat.id.lowker.Rest;

import lowker.nasiqulibat.id.lowker.Models.GetKategori;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Nasiqul on 2/21/2018.
 */

public interface ApiKategori {
    @GET("kategori")
    Call<GetKategori> getKtg();
}
