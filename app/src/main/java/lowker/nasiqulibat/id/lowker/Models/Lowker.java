package lowker.nasiqulibat.id.lowker.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Nasiqul on 2/12/2018.
 */

public class Lowker {
    @SerializedName("id_pekerjaan")
    @Expose
    private String id;
    @SerializedName("nama_pekerjaan")
    @Expose
    private String nama;
    @SerializedName("nama_kategori")
    @Expose
    private String kategori;
    @SerializedName("deskripsi")
    @Expose
    private String deskripsi;
    @SerializedName("nama_perusahaan")
    @Expose
    private String nama_perusahaan;
    @SerializedName("gaji")
    @Expose
    private String gaji;
    @SerializedName("pengalaman")
    @Expose
    private String pengalaman;
    @SerializedName("link")
    @Expose
    private String link;
    @SerializedName("tanggal")
    @Expose
    private String tgl;
    @SerializedName("profil")
    @Expose
    private String foto;
    @SerializedName("valid_tanggal")
    @Expose
    private String valid_tangal;


    public Lowker(String id, String nama, String kategori, String deskripsi, String nama_perusahaan, String gaji, String pengalaman, String link, String tgl, String foto, String valid) {
        this.id = id;
        this.nama = nama;
        this.kategori = kategori;
        this.deskripsi = deskripsi;
        this.nama_perusahaan = nama_perusahaan;
        this.gaji = gaji;
        this.pengalaman = pengalaman;
        this.link = link;
        this.tgl = tgl;
        this.foto = foto;
        this.valid_tangal = valid;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getKategori() {
        return kategori;
    }

    public void setKategori(String kategori) {
        this.kategori = kategori;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getNama_post() {
        return nama_perusahaan;
    }

    public void setNama_post(String nama_perusahaan) {
        this.nama_perusahaan = nama_perusahaan;
    }

    public String getGaji() {
        return gaji;
    }

    public void setGaji(String gaji) {
        this.gaji = gaji;
    }

    public String getPengalaman() {
        return pengalaman;
    }

    public void setPengalaman(String pengalaman) {
        this.pengalaman = pengalaman;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getTanggal() {
        return tgl;
    }

    public void setTanggal(String tgl) {
        this.tgl = tgl;
    }

    public String getProfil() {
        return foto;
    }

    public void setProfil(String foto) {
        this.foto = foto;
    }

    public String getValid() {
        return valid_tangal;
    }

    public void setValid(String valid) {
        this.valid_tangal = valid;
    }

}
