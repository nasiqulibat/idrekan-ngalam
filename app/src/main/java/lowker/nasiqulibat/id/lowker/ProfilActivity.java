package lowker.nasiqulibat.id.lowker;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.ButterKnife;
import lowker.nasiqulibat.id.lowker.Adapter.LowkerAdapter;
import lowker.nasiqulibat.id.lowker.Models.GetLowker;
import lowker.nasiqulibat.id.lowker.Models.GetUser;
import lowker.nasiqulibat.id.lowker.Models.Lowker;
import lowker.nasiqulibat.id.lowker.Models.PostPutDelLowker;
import lowker.nasiqulibat.id.lowker.Models.PostPutDelUser;
import lowker.nasiqulibat.id.lowker.Models.User;
import lowker.nasiqulibat.id.lowker.Rest.ApiClient;
import lowker.nasiqulibat.id.lowker.Rest.ApiInterface;
import lowker.nasiqulibat.id.lowker.Rest.ApiKategori;
import lowker.nasiqulibat.id.lowker.Rest.Session;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfilActivity extends AppCompatActivity {

    ApiInterface mApiInterface;
    EditText edtNama1, edtPerusahaan1, edtAlamat1, edtEmail1, edtTelp1;
    Context mContext;
    String getPos, id;
    Button btnEdit;
    Session session;
    TextView tvUsername;
    ImageView imgP;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profil);

        edtNama1 = (EditText) findViewById(R.id.edtNama);
        edtPerusahaan1 = (EditText) findViewById(R.id.edtPerusahaan);
        edtAlamat1 = (EditText) findViewById(R.id.edtAlamat);
        edtEmail1 = (EditText) findViewById(R.id.edtEmail);
        edtTelp1 = (EditText) findViewById(R.id.edtTelp);

        View header = findViewById(R.id.IC1);

        tvUsername = (TextView) header.findViewById(R.id.tvUsername);
        imgP = (ImageView) header.findViewById(R.id.imgProfil);


        session = new Session(getApplicationContext());
        mApiInterface = ApiClient.getClient().create(ApiInterface.class);

        mContext = this;
        id = session.getid_user();
        Picasso.with(mContext).load("http://idrekan-ngalam.000webhostapp.com/assets/upload/" + session.get_profil()).into(imgP);
        tvUsername.setText(session.getusename());


        Call<GetUser> userCall = mApiInterface.getUserId(id);
        userCall.enqueue(new Callback<GetUser>() {
            @Override
            public void onResponse(Call<GetUser> call, Response<GetUser>
                    response) {
                List<User> UserList = response.body().getListDataUser();
                edtNama1.setText(UserList.get(0).getNamaU());
                edtPerusahaan1.setText(UserList.get(0).getNamaP());
                edtAlamat1.setText(UserList.get(0).getAlamat());
                edtEmail1.setText(UserList.get(0).getEmail());
                edtTelp1.setText(UserList.get(0).getNo_telp());
            }

            @Override
            public void onFailure(Call<GetUser> call, Throwable t) {
                Log.e("Retrofit Get", t.toString());
            }
        });


        btnEdit = (Button) findViewById(R.id.btnEdit);
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtNama1.getText().toString().equals("")) {
                    Toast.makeText(mContext, "Nama Wajib Di Isi!", Toast.LENGTH_SHORT).show();
                } else if (edtPerusahaan1.getText().toString().equals("")) {
                    Toast.makeText(mContext, "Nama Perusahaan Wajib Di Isi!", Toast.LENGTH_SHORT).show();
                } else if (edtAlamat1.getText().toString().equals("")) {
                    Toast.makeText(mContext, "Alamat Wajib Di Isi!", Toast.LENGTH_SHORT).show();
                } else if (edtEmail1.getText().toString().equals("")) {
                    Toast.makeText(mContext, "Email Wajib Di Isi!", Toast.LENGTH_SHORT).show();
                } else if (edtTelp1.getText().toString().equals("")) {
                    Toast.makeText(mContext, "Nomor Telepon Wajib Di Isi!", Toast.LENGTH_SHORT).show();
                } else {

                    Call<PostPutDelUser> editUserCall = mApiInterface.putUser(
                            id,
                            edtNama1.getText().toString(),
                            edtPerusahaan1.getText().toString(),
                            edtAlamat1.getText().toString(),
                            edtTelp1.getText().toString(),
                            edtEmail1.getText().toString());
                    editUserCall.enqueue(new Callback<PostPutDelUser>() {
                        @Override
                        public void onResponse(Call<PostPutDelUser> call, Response<PostPutDelUser> response) {
                            Toast.makeText(mContext, "Data berhasil diupdate", Toast.LENGTH_SHORT).show();

                            Intent inten = new Intent(ProfilActivity.this, Main3Activity.class);
                            finish();
                            startActivity(inten);
                        }

                        @Override
                        public void onFailure(Call<PostPutDelUser> call, Throwable t) {
                            Toast.makeText(mContext, "Error", Toast.LENGTH_SHORT).show();
                        }
                    });
                }


            }
        });
    }
}
