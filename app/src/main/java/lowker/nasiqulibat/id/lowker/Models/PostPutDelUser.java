package lowker.nasiqulibat.id.lowker.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Nasiqul on 2/12/2018.
 */

public class PostPutDelUser {
    @SerializedName("status")
    String status;
    @SerializedName("result")
    User mUser;
    @SerializedName("message")
    String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public User getUser() {
        return mUser;
    }

    public void setUser(User user) {
        mUser = user;
    }
}
