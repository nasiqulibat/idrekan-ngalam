package lowker.nasiqulibat.id.lowker.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Kategori {
    @SerializedName("id_kategori")
    @Expose
    private String id_kategori;
    @SerializedName("nama_kategori")
    @Expose
    private String nama_kategori;

    public Kategori(String id_ktg, String nama_kategori) {
        this.id_kategori = id_ktg;
        this.nama_kategori = nama_kategori;
    }

    public String getIdKtg() {
        return id_kategori;
    }

    public void setIdKtg(String id_ktg) {
        this.id_kategori = id_ktg;
    }

    public String getNama_kategori() {
        return nama_kategori;
    }

    public void setNama_kategori(String nama_kategori) {
        this.nama_kategori = nama_kategori;
    }
}
