package lowker.nasiqulibat.id.lowker.Rest;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Nasiqul on 2/12/2018.
 */

public class ApiClient {
    public static final String BASE_URL = "http://idrekan-ngalam.000webhostapp.com/index.php/";
    private static Retrofit retrofit = null;

    public static Retrofit getClient() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
