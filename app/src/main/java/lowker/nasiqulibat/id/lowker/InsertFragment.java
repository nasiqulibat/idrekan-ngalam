package lowker.nasiqulibat.id.lowker;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import lowker.nasiqulibat.id.lowker.Models.GetKategori;
import lowker.nasiqulibat.id.lowker.Models.Kategori;
import lowker.nasiqulibat.id.lowker.Models.PostPutDelLowker;
import lowker.nasiqulibat.id.lowker.Rest.ApiClient;
import lowker.nasiqulibat.id.lowker.Rest.ApiInterface;
import lowker.nasiqulibat.id.lowker.Rest.ApiKategori;
import lowker.nasiqulibat.id.lowker.Rest.Session;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 * create an instance of this fragment.
 */
public class InsertFragment extends Fragment {

    @BindView(R.id.spinner)
    Spinner spinnerKtg;
    EditText edtNama, edtDesc, edtPengalaman, edtLink, edtGaji;
    ApiKategori mApiKategori;
    Context mContext;
    Button btInsert;
    ApiInterface mApiInterface;
    Session session;
    String id_user, getPos;

    public InsertFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_insert, container, false);

        edtNama = (EditText) rootView.findViewById(R.id.edtNama);
        edtDesc = (EditText) rootView.findViewById(R.id.edtDesc);
        edtPengalaman = (EditText) rootView.findViewById(R.id.edtPengalaman);
        edtLink = (EditText) rootView.findViewById(R.id.edtLink);
        edtGaji = (EditText) rootView.findViewById(R.id.edtGaji);
        session = new Session(getActivity());
        id_user = session.getid_user();

        mApiInterface = ApiClient.getClient().create(ApiInterface.class);
        mApiKategori = ApiClient.getClient().create(ApiKategori.class);
        ButterKnife.bind(this, rootView);
        mContext = this.getActivity();

        initSpinerKtg();

        btInsert = (Button) rootView.findViewById(R.id.btInserting);
        btInsert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edtNama.getText().toString().equals("")) {
                    Toast.makeText(getActivity(), "Nama Pekerjaan Wajib Di Isi!", Toast.LENGTH_SHORT).show();
                } else if (edtDesc.getText().toString().equals("")) {
                    Toast.makeText(getActivity(), "Deskripsi Wajib Di Isi!", Toast.LENGTH_SHORT).show();
                } else if (edtPengalaman.getText().toString().equals("")) {
                    Toast.makeText(getActivity(), "Pengalaman Wajib Di Isi!", Toast.LENGTH_SHORT).show();
                } else if (edtGaji.getText().toString().equals("")) {
                    Toast.makeText(getActivity(), "Gaji Wajib Di Isi!", Toast.LENGTH_SHORT).show();
                } else {
                    Call<PostPutDelLowker> postLowkerCall = mApiInterface.postLowker(edtNama.getText().toString(), edtPengalaman.getText().toString(), edtGaji.getText().toString(), getPos, edtDesc.getText().toString(), id_user, edtLink.getText().toString());
                    postLowkerCall.enqueue(new Callback<PostPutDelLowker>() {
                        @Override
                        public void onResponse(Call<PostPutDelLowker> call, Response<PostPutDelLowker> response) {
                            Toast.makeText(getActivity().getApplicationContext(), "Data Berhasil ditambahkan", Toast.LENGTH_SHORT).show();
                            IklanSayaFragment iklanSayaFragment = new IklanSayaFragment();
                            FragmentManager manager = getFragmentManager();
                            manager.beginTransaction().replace(R.id.cm3_fragment, iklanSayaFragment).commit();
                        }

                        @Override
                        public void onFailure(Call<PostPutDelLowker> call, Throwable t) {
                            Toast.makeText(getActivity().getApplicationContext(), "Error", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });

        return rootView;
    }

    public void initSpinerKtg() {
        mApiKategori.getKtg().enqueue(new Callback<GetKategori>() {
            @Override
            public void onResponse(Call<GetKategori> call, Response<GetKategori> response) {
                List<Kategori> ktgItems = response.body().getListDataKtg();
                List<String> listSpinner = new ArrayList<>();
                for (int i = 0; i < ktgItems.size(); i++) {
                    listSpinner.add(ktgItems.get(i).getNama_kategori());
                }

                ArrayAdapter<String> adapter = new ArrayAdapter<>(mContext,
                        android.R.layout.simple_spinner_item, listSpinner);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinnerKtg.setAdapter(adapter);

                spinnerKtg.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        int selectedItem = parent.getSelectedItemPosition() + 1;

                        getPos = String.valueOf(selectedItem);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }

            @Override
            public void onFailure(Call<GetKategori> call, Throwable t) {
                Toast.makeText(mContext, "Koneksi internet bermasalah", Toast.LENGTH_SHORT).show();
            }
        });
    }

}