package lowker.nasiqulibat.id.lowker;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.graphics.Matrix;
import android.os.Environment;
import android.provider.CalendarContract;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import lowker.nasiqulibat.id.lowker.Models.PostPutDelUser;
import lowker.nasiqulibat.id.lowker.Rest.ApiClient;
import lowker.nasiqulibat.id.lowker.Rest.ApiInterface;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DaftarActivity extends AppCompatActivity {

    String imagePath = "";
    Context mContext;
    Button btnDaftar;
    ApiInterface mApiInterface;
    EditText edtNama, edtUsername, edtPassword, edtPassword2, edtNamaP, edtAlamat, edtEmail, edtNotelp;
    ImageButton imgUpload;
    ProgressDialog pd;

    File mFileUri;

    Bitmap originalImage;
    int width;
    int height;
    int newWidth = 150;
    int newHeight = 150;
    Matrix matrix;
    Bitmap resizedBitmap;
    float scaleWidth;
    float scaleHeight;
    ByteArrayOutputStream outputStream;

    private static File getMediaFileName() {
        // Lokasi External sdcard
        File mediaStorageDir = new
                File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                "CameraDemo");
        // Buat directori tidak dire10ktori tidak eksis
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("CameraDemo", "Gagal membuat directory" + "CameraDemo");
                return null;
            }
        }
        File mediaFile = null;
        // Membuat nama file
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG_" + timeStamp
                + ".jpg");
        return mediaFile;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar);

        pd = new ProgressDialog(this);
        edtNama = (EditText) findViewById(R.id.edtNama);
        edtUsername = (EditText) findViewById(R.id.edtUsername);
        edtPassword = (EditText) findViewById(R.id.edtPassword);
        edtPassword2 = (EditText) findViewById(R.id.edtPassword2);
        edtNamaP = (EditText) findViewById(R.id.edtNamaP);
        edtAlamat = (EditText) findViewById(R.id.edtAlamat);
        edtEmail = (EditText) findViewById(R.id.edtEmail);
        edtNotelp = (EditText) findViewById(R.id.edtTelp);
        imgUpload = (ImageButton) findViewById(R.id.imageButton);

        mApiInterface = ApiClient.getClient().create(ApiInterface.class);

        mContext = this;

        btnDaftar = (Button) findViewById(R.id.btnDaftar);
        btnDaftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!edtPassword.getText().toString().equals(edtPassword2.getText().toString())) {
                    Toast.makeText(DaftarActivity.this, "Password harus sama", Toast.LENGTH_SHORT).show();
                } else {
                    pd.setMessage("Loading...");
                    pd.setCancelable(false);
                    pd.show();
                    MultipartBody.Part body = null;

                    if (!imagePath.isEmpty()) {
                        File file = new File(imagePath);

                        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);

                        body = MultipartBody.Part.createFormData("profil", file.getName(), requestFile);
                    }

                    RequestBody username = MultipartBody.create(MediaType.parse("multipart/form-data"), edtUsername.getText().toString());
                    RequestBody nama = MultipartBody.create(MediaType.parse("multipart/form-data"), edtNama.getText().toString());
                    RequestBody password = MultipartBody.create(MediaType.parse("multipart/form-data"), edtPassword.getText().toString());
                    RequestBody namaP = MultipartBody.create(MediaType.parse("multipart/form-data"), edtNamaP.getText().toString());
                    RequestBody alamat = MultipartBody.create(MediaType.parse("multipart/form-data"), edtAlamat.getText().toString());
                    RequestBody email = MultipartBody.create(MediaType.parse("multipart/form-data"), edtEmail.getText().toString());
                    RequestBody telp = MultipartBody.create(MediaType.parse("multipart/form-data"), edtNotelp.getText().toString());


                    Call<PostPutDelUser> postUserCall = mApiInterface.postUser(
                            username,
                            password,
                            nama,
                            namaP,
                            alamat,
                            telp,
                            email,
                            body
                    );
                    postUserCall.enqueue(new Callback<PostPutDelUser>() {
                        @Override
                        public void onResponse(Call<PostPutDelUser> call, Response<PostPutDelUser> response) {
                            Log.d("Insert", " Retrofit Insert: Berhasil Yess");
                            Toast.makeText(getApplicationContext(), "Data berhasil ditambah", Toast.LENGTH_LONG).show();
                            Intent inten = new Intent(DaftarActivity.this, LoginActivity.class);
                            finish();
                            startActivity(inten);
                        }

                        @Override
                        public void onFailure(Call<PostPutDelUser> call, Throwable t) {
                            Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        });

        imgUpload.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                selectImage();
            }
        });
    }

    private void selectImage() {
        final CharSequence[] items = {"Camera", "Gallery", "Batal"};

        AlertDialog.Builder builder = new AlertDialog.Builder(DaftarActivity.this);
        builder.setTitle("Upload Foto");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (items[i].equals("Camera")) {

                    captureImage();

                } else if (items[i].equals("Gallery")) {

                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(intent.createChooser(intent, "Pilih Gambar"), 2);

                } else if (items[i].equals("Batal")) {

                    dialogInterface.dismiss();
                }
            }
        }).show();
    }

    private void captureImage() {
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            mFileUri = getMediaFileName();
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, CalendarContract.CalendarCache.URI.fromFile(mFileUri));
            startActivityForResult(takePictureIntent, 1);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == 1) {
                originalImage = BitmapFactory.decodeFile(mFileUri.getPath());

                width = originalImage.getWidth();
                height = originalImage.getHeight();

                matrix = new Matrix();

                scaleWidth = ((float) newWidth) / width;

                scaleHeight = ((float) newHeight) / height;

                matrix.postScale(scaleWidth, scaleHeight);

                matrix.postRotate(0);

                resizedBitmap = Bitmap.createBitmap(originalImage, 0, 0, width, height, matrix, true);
//            outputStream = new ByteArrayOutputStream();

                FileOutputStream out = null;
                String filename = mFileUri.getPath();

                try {
                    out = new FileOutputStream(filename);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                resizedBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);

                Picasso.with(getApplicationContext()).load(new File(mFileUri.getPath())).into(imgUpload);
                Log.d("cek lokasi : ", "" + mFileUri.getPath());

                imagePath = mFileUri.getPath();

            } else if (requestCode == 2) {
                if (data == null) {
                    Toast.makeText(getApplicationContext(), "Gambar Gagal Di load",
                            Toast.LENGTH_LONG).show();
                    return;
                }

                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);

                if (cursor != null) {
                    cursor.moveToFirst();
                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    imagePath = cursor.getString(columnIndex);

                    Picasso.with(getApplicationContext()).load(new File(imagePath)).fit().into(imgUpload);
                    cursor.close();
                } else {
                    Toast.makeText(getApplicationContext(), "Gambar Gagal Di load",
                            Toast.LENGTH_LONG).show();
                }
            }
        }
    }
}
