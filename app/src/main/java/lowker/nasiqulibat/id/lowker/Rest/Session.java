package lowker.nasiqulibat.id.lowker.Rest;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by Andhika Adjie on 2/15/2018.
 */

public class Session {
    private SharedPreferences prefs;

    public Session(Context cntx) {
        // TODO Auto-generated constructor stub
        prefs = PreferenceManager.getDefaultSharedPreferences(cntx);
    }

    public void setusename(String usename) {
        prefs.edit().putString("usename", usename).commit();
    }

    public void setid_user(String id_user) {
        prefs.edit().putString("id_user", id_user).commit();
    }

    public void setprofil(String profil) {
        prefs.edit().putString("profil", profil).commit();
    }

    public String getusename() {
        String usename = prefs.getString("usename", "");
        return usename;
    }

    public String getid_user() {
        String id_user = prefs.getString("id_user", "");
        return id_user;
    }

    public String get_profil() {
        String profil = prefs.getString("profil", "");
        return profil;
    }
}
