package lowker.nasiqulibat.id.lowker;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.text.NumberFormat;

public class DetailsActivity extends AppCompatActivity {

    TextView tvNama, tvGaji, tvPengalaman, tvBidang, tvDesc, tvJudul;
    Button btnSelengkapnya;
    ImageView imgProfil;
    String link, profil;
    Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        tvJudul = (TextView) findViewById(R.id.tvJudul);
        tvNama = (TextView) findViewById(R.id.tvNama);
        tvGaji = (TextView) findViewById(R.id.tvGaji);
        tvPengalaman = (TextView) findViewById(R.id.tvPengalaman);
        tvBidang = (TextView) findViewById(R.id.tvBagian);
        tvDesc = (TextView) findViewById(R.id.tvInfo);
        imgProfil = (ImageView) findViewById(R.id.logo);

        mContext = this;
        Intent mIntent = getIntent();

        profil = mIntent.getStringExtra("profil");
        Picasso.with(mContext).load("http://idrekan-ngalam.000webhostapp.com/assets/upload/" + profil).into(imgProfil);
        tvJudul.setText(mIntent.getStringExtra("nama_pekerjaan"));
        tvNama.setText(mIntent.getStringExtra("nama_Post"));

        NumberFormat formatter2 = new DecimalFormat("#.###");

        Double gaji2 = Double.parseDouble(mIntent.getStringExtra("gaji"));
        String formattedNumber = formatter2.format(gaji2);

        tvGaji.setText("Rp " + formattedNumber + " / bulan");

        if (mIntent.getStringExtra("pengalaman").equals("0"))
            tvPengalaman.setText("Fresh Graduate");
        else
            tvPengalaman.setText(mIntent.getStringExtra("pengalaman") + " tahun pengalaman");

        tvBidang.setText(mIntent.getStringExtra("kategori"));
        tvDesc.setText(mIntent.getStringExtra("deskripsi"));
        link = mIntent.getStringExtra("link");

    }

    public void Goto(View v)
    {
        Uri uriUrl = Uri.parse("http://" + link);
        Intent viewIntent = new Intent(Intent.ACTION_VIEW, uriUrl);
        startActivity(viewIntent);
    }
}
