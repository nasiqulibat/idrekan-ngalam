package lowker.nasiqulibat.id.lowker;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import lowker.nasiqulibat.id.lowker.Models.GetKategori;
import lowker.nasiqulibat.id.lowker.Models.Kategori;
import lowker.nasiqulibat.id.lowker.Rest.ApiClient;
import lowker.nasiqulibat.id.lowker.Rest.ApiKategori;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FilterActivity extends AppCompatActivity {

    @BindView(R.id.spinner2)
    Spinner spinnerKtg;
    ApiKategori mApiKategori;
    EditText edtCari;
    Context mContext;
    RadioButton radiobutton,rbterbaru;
    Button btnFilter;
    String strText, getPos;
    RadioGroup rg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);

        edtCari = (EditText) findViewById(R.id.edtCari);
        rbterbaru = (RadioButton) findViewById(R.id.rBUrutkan1);
        rbterbaru.setSelected(true);

        rg = (RadioGroup) findViewById(R.id.rGUrut);

        Intent inten = getIntent();
        strText = inten.getExtras().getString("string");

        edtCari.setText(strText);

        mApiKategori = ApiClient.getClient().create(ApiKategori.class);
        ButterKnife.bind(this);
        mContext = this;

        initSpinerKtg();

        btnFilter = (Button) findViewById(R.id.btnCari);
        btnFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int selectedId = rg.getCheckedRadioButtonId();

                // find the radiobutton by returned id
                radiobutton = (RadioButton) findViewById(selectedId);

                Intent i = new Intent(FilterActivity.this, Main3Activity.class);
                i.putExtra("keyword", edtCari.getText().toString());
                i.putExtra("kategori", getPos);
                i.putExtra("sort", radiobutton.getText().toString());
                i.putExtra("fragment", 1);

                finish();
                startActivity(i);
            }
        });
    }

    public void initSpinerKtg() {
        mApiKategori.getKtg().enqueue(new Callback<GetKategori>() {
            @Override
            public void onResponse(Call<GetKategori> call, Response<GetKategori> response) {
                List<Kategori> ktgItems = response.body().getListDataKtg();
                List<String> listSpinner = new ArrayList<>();
                for (int i = 0; i < ktgItems.size(); i++) {
                    listSpinner.add(ktgItems.get(i).getNama_kategori());
                }

                ArrayAdapter<String> adapter = new ArrayAdapter<>(mContext,
                        android.R.layout.simple_spinner_item, listSpinner);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinnerKtg.setAdapter(adapter);

                spinnerKtg.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        int selectedItem = parent.getSelectedItemPosition() + 1;

                        getPos = String.valueOf(selectedItem);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }

            @Override
            public void onFailure(Call<GetKategori> call, Throwable t) {
                Toast.makeText(mContext, "Koneksi internet bermasalah", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
