package lowker.nasiqulibat.id.lowker.Adapter;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import lowker.nasiqulibat.id.lowker.DetailsActivity;
import lowker.nasiqulibat.id.lowker.Models.Lowker;
import lowker.nasiqulibat.id.lowker.R;

/**
 * Created by Nasiqul on 2/13/2018.
 */

public class LowkerAdapter extends RecyclerView.Adapter<LowkerAdapter.MyViewHolder> {
    List<Lowker> mLowkerList;
    //private Context context2;

    public LowkerAdapter(List<Lowker> lowkerList) {
        mLowkerList = lowkerList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.listview, parent, false);
        MyViewHolder mViewHolder = new MyViewHolder(mView);
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.mTextViewNama.setText(mLowkerList.get(position).getNama());
        holder.mTextViewKategori.setText(mLowkerList.get(position).getKategori());

        NumberFormat formatter = new DecimalFormat("#,###");

        Double gaji = Double.parseDouble(mLowkerList.get(position).getGaji());
        String formattedNumber = formatter.format(gaji);

        holder.mTextViewGaji.setText("Rp " + formattedNumber + " /bulan");

        if (mLowkerList.get(position).getPengalaman().equals("0"))
            holder.mTextViewPengalaman.setText("Fresh Graduate");
        else
            holder.mTextViewPengalaman.setText("Min. " + mLowkerList.get(position).getPengalaman() + " tahun pengalaman");

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        String now = dateFormat.format(calendar.getTime());

        Date convertedDate = new Date();
        Date dateNow = new Date();
        try {
            dateNow = dateFormat.parse(now);
            convertedDate = dateFormat.parse(mLowkerList.get(position).getTanggal());
        } catch (ParseException e) {

            e.printStackTrace();
        }

        long diff = dateNow.getTime() - convertedDate.getTime();

        if (diff == 0)
            holder.mTextViewTgl.setText("diinputkan Hari ini");
        else if(diff == 1)
            holder.mTextViewTgl.setText("diinputkan Kemarin");
        else
            holder.mTextViewTgl.setText("diinputkan " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS) + " hari yang lalu");

        holder.mTextViewValid.setText(mLowkerList.get(position).getValid());
        Picasso.with(holder.itemView.getContext()).load("http://idrekan-ngalam.000webhostapp.com/assets/upload/" + mLowkerList.get(position).getProfil()).into(holder.mImageViewFoto);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mIntent = new Intent(view.getContext(), DetailsActivity.class);
                mIntent.putExtra("nama_pekerjaan", mLowkerList.get(position).getNama());
                mIntent.putExtra("kategori", mLowkerList.get(position).getKategori());
                mIntent.putExtra("deskripsi", mLowkerList.get(position).getDeskripsi());
                mIntent.putExtra("nama_Post", mLowkerList.get(position).getNama_post());
                mIntent.putExtra("gaji", mLowkerList.get(position).getGaji());
                mIntent.putExtra("pengalaman", mLowkerList.get(position).getPengalaman());
                mIntent.putExtra("link", mLowkerList.get(position).getLink());
                mIntent.putExtra("profil", mLowkerList.get(position).getProfil());

                view.getContext().startActivity(mIntent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mLowkerList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView mTextViewNama, mTextViewGaji, mTextViewKategori, mTextViewPengalaman, mTextViewTgl, mTextViewValid;
        public ImageView mImageViewFoto;

        public MyViewHolder(View itemView) {
            super(itemView);
            mTextViewNama = (TextView) itemView.findViewById(R.id.tvNama);
            mTextViewKategori = (TextView) itemView.findViewById(R.id.tvKtg);
            mTextViewGaji = (TextView) itemView.findViewById(R.id.tvGaji);
            mTextViewPengalaman = (TextView) itemView.findViewById(R.id.tvPengalaman);
            mTextViewTgl = (TextView) itemView.findViewById(R.id.tvTgl);
            mTextViewValid = (TextView) itemView.findViewById(R.id.tvValidOn);
            mImageViewFoto = (ImageView) itemView.findViewById(R.id.imgProfil);
        }
    }

}
