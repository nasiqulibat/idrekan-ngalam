package lowker.nasiqulibat.id.lowker.Rest;

import lowker.nasiqulibat.id.lowker.Models.GetLowker;
import lowker.nasiqulibat.id.lowker.Models.GetUser;
import lowker.nasiqulibat.id.lowker.Models.PostPutDelLowker;
import lowker.nasiqulibat.id.lowker.Models.PostPutDelUser;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.HTTP;
import retrofit2.http.Part;
import retrofit2.http.Query;

/**
 * Created by Nasiqul on 2/12/2018.
 */

public interface ApiInterface {
    @GET("pekerjaan")
    Call<GetLowker> getLowker();

    @GET("pekerjaan/getUser")
    Call<GetLowker> getLowkerGet(@Query("id_user") String id_user);

    @GET("pekerjaan/perusahaan")
    Call<GetLowker> getLowkerP(@Query("nama_pekerjaan") String nama);

    @GET("pekerjaan/search")
    Call<GetLowker> getLowkerSearch(@Query("nama_pekerjaan") String nama,
                                    @Query("kategori") String kategori,
                                    @Query("sort") String sort);

    @FormUrlEncoded
    @POST("pekerjaan")
    Call<PostPutDelLowker> postLowker(@Field("nama_pekerjaan") String nama,
                                      @Field("pengalaman") String pengalaman,
                                      @Field("gaji") String gaji,
                                      @Field("kategori") String kategori,
                                      @Field("deskripsi") String deskripsi,
                                      @Field("nama_post") String nama_post,
                                      @Field("link") String link);

    @FormUrlEncoded
    @POST("pekerjaan/update")
    Call<PostPutDelLowker> putLowker(@Field("id_pekerjaan") String id,
                                     @Field("nama_pekerjaan") String nama,
                                     @Field("pengalaman") String pengalaman,
                                     @Field("gaji") String gaji,
                                     @Field("kategori") String kategori,
                                     @Field("deskripsi") String deskripsi,
                                     @Field("link") String link);

    @FormUrlEncoded
    @HTTP(method = "DELETE", path = "pekerjaan", hasBody = true)
    Call<PostPutDelLowker> deleteLowker(@Field("id_pekerjaan") String id_pekerjaan);

    @Multipart
    @POST("user")
    Call<PostPutDelUser> postUser(@Part("username") RequestBody username,
                                  @Part("password") RequestBody password,
                                  @Part("nama") RequestBody nams,
                                  @Part("nama_perusahaan") RequestBody nama_perusahaan,
                                  @Part("alamat") RequestBody alamat,
                                  @Part("no_telp") RequestBody no_telp,
                                  @Part("email") RequestBody email,
                                  @Part MultipartBody.Part profil);

    @FormUrlEncoded
    @POST("user/user")
    Call<PostPutDelUser> putUser(@Field("id_user") String id_user,
                                     @Field("nama") String nama,
                                     @Field("nama_perusahaan") String nama_perusahaan,
                                     @Field("alamat") String alamat,
                                     @Field("no_telp") String no_telp,
                                     @Field("email") String email);

    @GET("user/getId")
    Call<GetUser> getUserId(@Query("id_user") String id);
}
