package lowker.nasiqulibat.id.lowker;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import lowker.nasiqulibat.id.lowker.Adapter.LowkerAdapter;
import lowker.nasiqulibat.id.lowker.Models.GetLowker;
import lowker.nasiqulibat.id.lowker.Models.Lowker;
import lowker.nasiqulibat.id.lowker.Rest.ApiClient;
import lowker.nasiqulibat.id.lowker.Rest.ApiInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class Search2Fragment extends Fragment {

    public static Search2Fragment s2f;
    ApiInterface mApiInterface;
    String keyword, kategori, sort;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    public Search2Fragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        keyword = getArguments().getString("keyword");
        kategori = getArguments().getString("kategori");
        sort = getArguments().getString("sort");
        View rootView = inflater.inflate(R.layout.fragment_search2, container, false);

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.RC4);

        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mApiInterface = ApiClient.getClient().create(ApiInterface.class);
        s2f = this;
        refresh();

        return rootView;
    }

    public void refresh() {
        Call<GetLowker> lowkerCall = mApiInterface.getLowkerSearch(keyword, kategori, sort);
        lowkerCall.enqueue(new Callback<GetLowker>() {
            @Override
            public void onResponse(Call<GetLowker> call, Response<GetLowker>
                    response) {
                List<Lowker> LowkerList = response.body().getListDataLowker();
                Log.d("Retrofit Get", "Jumlah data Pekerjaan: " +
                        String.valueOf(LowkerList.size()));
                mAdapter = new LowkerAdapter(LowkerList);
                mRecyclerView.setAdapter(mAdapter);
            }

            @Override
            public void onFailure(Call<GetLowker> call, Throwable t) {
                Log.e("Retrofit Get", t.toString());
            }
        });
    }

}
