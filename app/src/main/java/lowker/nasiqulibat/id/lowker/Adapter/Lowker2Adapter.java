package lowker.nasiqulibat.id.lowker.Adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import lowker.nasiqulibat.id.lowker.EditFragment;
import lowker.nasiqulibat.id.lowker.Main3Activity;
import lowker.nasiqulibat.id.lowker.Models.Lowker;
import lowker.nasiqulibat.id.lowker.R;

/**
 * Created by Nasiqul on 2/13/2018.
 */

public class Lowker2Adapter extends RecyclerView.Adapter<Lowker2Adapter.MyViewHolder> {
    List<Lowker> mLowkerList;
    Context context;
    String id;

    public Lowker2Adapter(List<Lowker> lowkerList, FragmentActivity context) {
        mLowkerList = lowkerList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.listview2, parent, false);
        MyViewHolder mViewHolder = new MyViewHolder(mView);
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        holder.mTextViewId.setText(mLowkerList.get(position).getId());
        holder.mTextViewNama.setText(mLowkerList.get(position).getNama());
        holder.mTextViewKategori.setText(mLowkerList.get(position).getKategori());

        NumberFormat formatter3 = new DecimalFormat("#.###");

        Double gaji3 = Double.parseDouble(mLowkerList.get(position).getGaji());
        String formattedNumber = formatter3.format(gaji3);

        holder.mTextViewGaji.setText("Rp " + formattedNumber + " /bulan");

        if (mLowkerList.get(position).getPengalaman().equals("0"))
            holder.mTextViewPengalaman.setText("Fresh Graduate");
        else
            holder.mTextViewPengalaman.setText("min. " + mLowkerList.get(position).getPengalaman() + " tahun pengalaman");

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        String now = dateFormat.format(calendar.getTime());

        Date convertedDate = new Date();
        Date dateNow = new Date();
        try {
            dateNow = dateFormat.parse(now);
            convertedDate = dateFormat.parse(mLowkerList.get(position).getTanggal());
        } catch (ParseException e) {

            e.printStackTrace();
        }

        long diff = dateNow.getTime() - convertedDate.getTime();

        if (diff == 0)
            holder.mTextViewTgl.setText("diinputkan Hari ini");
        else if(diff == 1)
            holder.mTextViewTgl.setText("diinputkan Kemarin");
        else
            holder.mTextViewTgl.setText("diinputkan " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS) + " hari yang lalu");


        holder.mTextViewValid.setText(mLowkerList.get(position).getValid());
        Picasso.with(holder.itemView.getContext()).load("http://idrekan-ngalam.000webhostapp.com/assets/upload/" + mLowkerList.get(position).getProfil()).into(holder.mImageViewFoto);

        id = holder.mTextViewId.getText().toString();
        holder.mbtnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Main3Activity.ma3.delete(id);
            }
        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putString("id_pekerjaan", mLowkerList.get(position).getId());
                bundle.putString("nama_pekerjaan", mLowkerList.get(position).getNama());
                bundle.putString("kategori", mLowkerList.get(position).getKategori());
                bundle.putString("deskripsi", mLowkerList.get(position).getDeskripsi());
                bundle.putString("gaji", mLowkerList.get(position).getGaji());
                bundle.putString("pengalaman", mLowkerList.get(position).getPengalaman());
                bundle.putString("link", mLowkerList.get(position).getLink());

                // set Fragmentclass Arguments
                EditFragment editFragment = new EditFragment();
                editFragment.setArguments(bundle);

                FragmentManager manager = ((Main3Activity) context).getSupportFragmentManager();
                manager.beginTransaction().add(R.id.cm3_fragment, editFragment).commit();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mLowkerList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView mTextViewId, mTextViewNama, mTextViewGaji, mTextViewKategori, mTextViewPengalaman, mTextViewTgl, mTextViewValid;
        public Button mbtnDelete;
        public ImageView mImageViewFoto;

        public MyViewHolder(View itemView) {
            super(itemView);
            mTextViewId = (TextView) itemView.findViewById(R.id.tvId);
            mTextViewNama = (TextView) itemView.findViewById(R.id.tvNama);
            mTextViewKategori = (TextView) itemView.findViewById(R.id.tvKtg);
            mTextViewGaji = (TextView) itemView.findViewById(R.id.tvGaji);
            mTextViewPengalaman = (TextView) itemView.findViewById(R.id.tvPengalaman);
            mTextViewTgl = (TextView) itemView.findViewById(R.id.tvTgl);
            mbtnDelete = (Button) itemView.findViewById(R.id.btnDel);
            mTextViewValid = (TextView) itemView.findViewById(R.id.tvValidOn);
            mImageViewFoto = (ImageView) itemView.findViewById(R.id.imgProfil);
        }
    }

}
