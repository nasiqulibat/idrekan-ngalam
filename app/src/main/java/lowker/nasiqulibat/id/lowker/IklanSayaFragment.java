package lowker.nasiqulibat.id.lowker;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import lowker.nasiqulibat.id.lowker.Adapter.Lowker2Adapter;
import lowker.nasiqulibat.id.lowker.Models.GetLowker;
import lowker.nasiqulibat.id.lowker.Models.Lowker;
import lowker.nasiqulibat.id.lowker.Rest.ApiClient;
import lowker.nasiqulibat.id.lowker.Rest.ApiInterface;
import lowker.nasiqulibat.id.lowker.Rest.Session;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class IklanSayaFragment extends Fragment {

    public static IklanSayaFragment ifsa;
    ApiInterface mApiInterface;
    Session session;
    String id;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    public IklanSayaFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_iklan_saya, container, false);

        session = new Session(getActivity());
        id = session.getid_user();

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.RC2);

        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mApiInterface = ApiClient.getClient().create(ApiInterface.class);
        ifsa = this;
        refresh();

        FloatingActionButton fab = (FloatingActionButton) rootView.findViewById(R.id.fab2);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InsertFragment insertFragment = new InsertFragment();
                FragmentManager manager = getFragmentManager();
                manager.beginTransaction().replace(R.id.cm3_fragment, insertFragment).commit();

            }
        });

        return rootView;
    }

    public void refresh() {
        Call<GetLowker> lowkerCall = mApiInterface.getLowkerGet(id);
        lowkerCall.enqueue(new Callback<GetLowker>() {
            @Override
            public void onResponse(Call<GetLowker> call, Response<GetLowker>
                    response) {
                List<Lowker> LowkerList = response.body().getListDataLowker();
                Log.d("Retrofit Get", "Jumlah data Pekerjaan: " +
                        String.valueOf(LowkerList.size()));
                mAdapter = new Lowker2Adapter(LowkerList, getActivity());
                mRecyclerView.setAdapter(mAdapter);
            }

            @Override
            public void onFailure(Call<GetLowker> call, Throwable t) {
                Log.e("Retrofit Get", t.toString());
            }
        });
    }

}
