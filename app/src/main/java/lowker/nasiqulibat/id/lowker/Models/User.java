package lowker.nasiqulibat.id.lowker.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User {
    @SerializedName("id_user")
    @Expose
    private String id_user;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("nama")
    @Expose
    private String nama;
    @SerializedName("nama_perusahaan")
    @Expose
    private String nama_perusahaan;
    @SerializedName("alamat")
    @Expose
    private String alamat;
    @SerializedName("no_telp")
    @Expose
    private String no_telp;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("profil")
    @Expose
    private String profil;

    public User(String username, String password, String nama, String nama_perusahaan, String alamat, String no_telp, String email, String profil) {
        this.username = username;
        this.password = password;
        this.nama = nama;
        this.nama_perusahaan = nama_perusahaan;
        this.alamat = alamat;
        this.no_telp = no_telp;
        this.email = email;
        this.profil = profil;
    }

    public String getIdUser() {
        return id_user;
    }

    public void setIdUser(String id_user) {
        this.id_user = id_user;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNamaU() {
        return nama;
    }

    public void setNamaU(String nama) {
        this.nama = nama;
    }

    public String getNamaP() {
        return nama_perusahaan;
    }

    public void setNamaP(String nama_perusahaan) {
        this.nama_perusahaan = nama_perusahaan;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getNo_telp() {
        return no_telp;
    }

    public void setNo_telp(String no_telp) {
        this.no_telp = no_telp;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProfil() {
        return profil;
    }

    public void setProfil(String profil) {
        this.profil = profil;
    }
}
