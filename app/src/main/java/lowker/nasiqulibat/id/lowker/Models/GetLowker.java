package lowker.nasiqulibat.id.lowker.Models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Nasiqul on 2/12/2018.
 */

public class GetLowker {
    @SerializedName("status")
    String status;
    @SerializedName("result")
    List<Lowker> listDataLowker;
    @SerializedName("message")
    String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Lowker> getListDataLowker() {
        return listDataLowker;
    }

    public void setListDataLowker(List<Lowker> listDataLowker) {
        this.listDataLowker = listDataLowker;
    }
}
